#!/usr/bin/env bash

# Trying to fix ssh-agent in tmux
# http://mah.everybody.org/docs/ssh
SSH_ENV="$HOME/.ssh/environment"

# Look in /usr/share/zoneinfo for zones
function big-clock() {
  local zone="${1}"; shift

  if ! command -v figlet &> /dev/null ;then
    echo "'figlet' could not be found"
    return 1
  else
    watch -tn 1 "echo ${zone} ; TZ=${zone} date +%H:%M | figlet -ctk"
  fi
}


function figlet-print() {
  printf "\033c"
  figlet -ctk "$1"
}

function countdown-timer() {
  local secs="${1}"; shift

  if [[ -z ${secs} ]]; then
    read -p 'seconds: ' secs
  fi

  while [ $secs -gt 0 ]; do
    figlet-print $secs
    sleep 1
    secs=$((secs-1))
  done
}

function start-agent() {
   echo "Initialising new SSH agent..."
   /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
   echo succeeded
   chmod 600 "${SSH_ENV}"
   . "${SSH_ENV}" > /dev/null
   /usr/bin/ssh-add;
}

# Source SSH settings, if applicable
function run-ssh-agent() {
  if [ -f "${SSH_ENV}" ]; then
     . "${SSH_ENV}" > /dev/null
     ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
         start-agent;
     }
  else
       start-agent;
  fi
}

function update-system() {
  sudo apt-get update && \
  sudo apt-get -y upgrade && \
  sudo apt-get -y dist-upgrade && \
  sudo apt-get -y autoremove && \
  sudo apt-get clean
}

# THis will rename all files in current directory
function remove-spaces-in-filename() {
  for file in *\ *; do mv "$file" "${file// /_}"; done
}

# reference
# https://stackoverflow.com/questions/27994033/how-to-get-a-list-of-internal-ip-addresses-of-gce-instances
function get_gcp_instance_ip() {
  local ext_int="${1}"; shift
  local gcp_instance="${1}"; shift

  case "${ext_int}" in
    i | int | internal)
      gcloud compute instances list \
        --format="value(networkInterfaces[0].networkIP)" \
        --filter "${gcp_instance}" ;;
    e | ext | external)
      gcloud compute instances list \
        --format="value(networkInterfaces[0].accessConfigs[0].natIP)" \
        --filter "${gcp_instance}" ;;
    *)
      echo "ERROR! No IP found" ;;
  esac

}

# remove ssh host keys that change with new GCP instances
# example: clear_gcp_ssh_host_keys "10.138.0."
function clear_gcp_ssh_host_keys() {
  local subnet="${1}"; shift

  for i in {0..255}; do
    echo $i
    ssh-keygen -f "/home/rcoran1/.ssh/known_hosts" -R "${subnet}$i"
  done
}

function onetab_to_list() {
  local file="${1}"; shift

  awk -F "|" '{print "["$2"]""("$1")"}' "${file}" |
  sed 's/|//2g' |
  sed 's/\(\)\[\]//g' |
  sed 's/\[ /\[/g' |
  sed 's/ )/)/g' |
  sed 's/^/- /g'
}

function onetab_to_checklist() {
  local file="${1}"; shift

  awk -F "|" '{print "["$2"]""("$1")"}' "${file}" |
  sed 's/|//2g' |
  sed 's/\(\)\[\]//g' |
  sed 's/\[ /\[/g' |
  sed 's/ )/)/g' |
  sed 's/^/- \[ \] /g'
}

#################################
# git functions
#################################

function git_remove_submodule() {
  local submodule="${1}"; shift

  git submodule deinit -f -- "${submodule}" ; \
  rm -rf ".git/modules/${submodule}" ; \
  git rm -f "${submodule}"
}

function gcm() {
  local prefix=$(
    git rev-parse --abbrev-ref HEAD |
    grep -oE '^[A-Za-z]+-[0-9]+' |
    tr '[:lower:]' '[:upper:]'
  )
  local commit_message
  if [[ -n "${@}" ]]; then
    commit_message="${@}"
  else
    read -p "Enter commit message: " commit_message
  fi
  git commit -m "${prefix} ${commit_message}"
}
