#!/usr/bin/env bash

# $1 = rows $2 = columns
set -- $(stty size)

tmux new-session -d -x "$2" -y "$(($1 - 1))" -t hud # status line uses a row
tmux split-window -h -t hud
tmux select-pane -t hud:1.1
tmux split-window -t hud
tmux select-pane -t hud:1.2
tmux split-window -t hud
tmux select-pane -t hud:1.0
tmux split-window -t hud
tmux select-pane -t hud:1.2
tmux split-window -h -t hud
tmux select-pane -t hud:1.2
tmux split-window -h -t hud

tmux send-keys -t hud:1.0 C-z 'cd ; sudo iftop -c ~/.iftoprc' Enter
tmux send-keys -t hud:1.1 C-z 'cd ; glances' Enter
tmux send-keys -t hud:1.2 C-z 'cd ; watch -n1 -t "date +%T ; stoken"' Enter
tmux send-keys -t hud:1.3 C-z 'cd ; big-clock UTC' Enter
tmux send-keys -t hud:1.4 C-z 'cd ; big-clock US/Pacific' Enter
tmux send-keys -t hud:1.5 C-z 'cd ; speedometer -r wlan0 -t wlan0' Enter
tmux send-keys -t hud:1.6 C-z 'cd ; ncspot' Enter

tmux resize-pane -t hud:1.2 -U 13
tmux resize-pane -t hud:1.6 -U 10
tmux resize-pane -t hud:1.2 -L 11
tmux resize-pane -t hud:1.3 -R 5

tmux a -t hud
