" Plugins
"Plugin 'scrooloose/syntastic'

" general changes
set expandtab
if has("autocmd")
    " If the filetype is Makefile then we need to use tabs
    " So do not expand tabs into space.
    autocmd FileType make   set noexpandtab
endif
set number
colo ron
syntax on
set softtabstop=2
set shiftwidth=2

set mouse=r
" Multi toggle
" https://jnrowe.github.io/articles/tips/Toggling_settings_in_vim.html
" Relative line numbers
" https://jeffkreeftmeijer.com/vim-number/
noremap <F3> :set number!<CR>
inoremap <F3> <C-O>:set number!<CR>

" Allow shift+tab to insert literal tab char for Makefiles
inoremap <S-Tab> <C-V><Tab>

" Automatically remove all trailing whitespace
" http://vim.wikia.com/wiki/Remove_unwanted_spaces
autocmd BufWritePre * %s/\s\+$//e

" Toggle color column at line 80
nnoremap <F4> :let &cc = &cc == '' ? '80' : ''<CR>

" Toggle highlight search
nmap <F2> :set hls! <cr>

" yaml 2 space tab after colon
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" paste toggle
set pastetoggle=<F5>

" code folding
"set foldmethod=syntax
"set foldcolumn=2
"set foldlevelstart=2

" Python IDE settings
"set textwidth=79  " lines longer than 79 columns will be broken
"set shiftwidth=4  " operation >> indents 4 columns; << unindents 4 columns
"set tabstop=4     " a hard TAB displays as 4 columns
"set expandtab     " insert spaces when hitting TABs
"set softtabstop=4 " insert/delete 4 spaces when hitting a TAB/BACKSPACE
"set shiftround    " round indent to multiple of 'shiftwidth'
set autoindent    " align the new line indent with the previous line

" vim-plug
call plug#begin()
" The default plugin directory will be as follows:
"   - Vim (Linux/macOS): '~/.vim/plugged'
"   - Vim (Windows): '~/vimfiles/plugged'
"   - Neovim (Linux/macOS/Windows): stdpath('data') . '/plugged'
" You can specify a custom plugin directory by passing it as the argument
"   - e.g. `call plug#begin('~/.vim/plugged')`
"   - Avoid using standard Vim directory names like 'plugin'

" Make sure you use single quotes

" On-demand loading
" Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }

" Initialize plugin system
" - Automatically executes `filetype plugin indent on` and `syntax enable`.
call plug#end()
" You can revert the settings after the call like so:
"   filetype indent off   " Disable file-type-specific indentation
"   syntax off            " Disable syntax highlighting
