# Randy Coran's dotfiles

## TODO
* [tmux active pane colors](http://www.deanbodenham.com/learn/tmux-pane-colours.html)
* [fzf fuzzy finder, bash, vim, tmux plugin](https://github.com/junegunn/fzf#fzf-tmux-script)

- [Examples](docs/EXAMPLES.md)
- [Vim / NeoVim](docs/VIM.md)
- [Tmux](docs/TMUX.md)
- [Git](docs/GIT.md)
- [WSL](docs/WSL.md)
