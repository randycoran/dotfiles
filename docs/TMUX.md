# TMUX

## Examples

https://github.com/rothgar/awesome-tmux

### tmux additions

[tmux plugin manager](https://github.com/tmux-plugins/tpm)
[tmux-ressurect](https://github.com/tmux-plugins/tmux-resurrect)
[tmux-continuum](https://github.com/tmux-plugins/tmux-continuum)
[tmux popup](https://old.reddit.com/r/tmux/comments/ol53gt/tmuxpopup/https://old.reddit.com/r/tmux/comments/ol53gt/tmuxpopup/)

[Youtube - Ttmux popup](https://www.youtube.com/watch?v=3TnTHR6lB7Y) - Need tmux 3.2+

### tmux discussions
https://news.ycombinator.com/item?id=23003603

[Just dig through this guy's github, goldmine](https://github.com/WaylonWalker?tab=repositories)
[WaylonWalker.com](https://waylonwalker.com )
[Reddit user 'waylonwalker'](https://old.reddit.com/user/_waylonwalker)

## Plugins

### Spotify

[tmux-plugin-spotify](https://github.com/pwittchen/tmux-plugin-spotify)
This si dependant on [spotify-cli-linux](https://github.com/pwittchen/spotify-cli-linux)

## Starting up a dashbaord from a script

[Respawning an existing pane with a specific command](https://superuser.com/questions/492266/run-or-send-a-command-to-a-tmux-pane-in-a-running-tmux-session)

Example commands:

```
# launch speedometer
tmux respawn-pane -t local:8.3 -k 'speedometer -r wlp111s0 -t wlp111s0'

# launch glances
tmux respawn-pane -t local:8.4 -k 'glances'
```

## Other dashbaord commands
* iftop
* glances
* vim sessions
* big-clock function (Need to use send-keys)
* speedometer
