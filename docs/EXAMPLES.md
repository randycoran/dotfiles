# Example dotfiles form others


[Lissy93 - Github](https://github.com/Lissy93/dotfiles) - this has a really nice README and arch vagrantfile.

### Example dotfiles from others to get inspiration from
https://github.com/ajmalsiddiqui/dotfiles

https://github.com/thoughtbot/dotfiles
https://github.com/christitustech/myvim

Recently switched to ansible
https://github.com/tangledhelix/dotfiles

#### Tmux for mere mortals
https://github.com/zserge/dotfiles
https://zserge.com/posts/tmux/
