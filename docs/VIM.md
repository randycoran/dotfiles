# Vim / NeoVim

## Vim to read

[Learn Vimscript the hard way](https://learnvimscriptthehardway.stevelosh.com/)

## Neovim

[rockerBOO/awesome-neovim](https://github.com/rockerBOO/awesome-neovim)

## Vim use literal tabs in Makefile

https://vi.stackexchange.com/questions/704/insert-tabs-in-insert-mode-when-expandtab-is-set

## Vim Plugins

[vim-plug](https://github.com/junegunn/vim-plug)

[akrawchyk/awesome-vim](https://github.com/akrawchyk/awesome-vim)
[vim-airline](https://github.com/vim-airline/vim-airline) / [vim-airline-themes](https://github.com/vim-airline/vim-airline-themes)

```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
```

* [UltiSnips](https://github.com/SirVer/ultisnips)
* [snipMate & UltiSnip Snippets](https://github.com/honza/vim-snippets)
* [Vim-EasyComplete](https://github.com/jayli/vim-easycomplete)
* [NERD commenter](https://github.com/preservim/nerdcommenter)
