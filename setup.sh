#!/usr/bin/env bash

set -euo pipefail  # err exit, nounset, any failure in pipe fails

DF_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

run_as_root=false
if [ $(id -u) -eq 0 ]; then
  run_as_root=true
fi

function check_and_install_apps() {
  local app_list=("$@")  # list of apps to check/install
  local missing_apps=()  # initialize array of missing apps

  # check if each app is installed and add missing apps to the array
  echo -n "Missing: "
  for app in "${app_list[@]}"; do
    if ! which "${app}" >> /dev/null ; then
      echo -n "${app} "
      missing_apps+=("${app}")
    fi
  done

  # install missing apps (if any)
  if [ ${#missing_apps[@]} -gt 0 ]; then
    echo "" ; echo -n "Installing..."
    if [ "${run_as_root}" = true ]; then
      apt-get -qq update && apt-get -qq install "${missing_apps[@]}" >/dev/null 2>&1
    else
      sudo apt-get -qq update && sudo apt-get -qq install "${missing_apps[@]}" >/dev/null 2>&1
    fi
    echo "done"
  else
    echo "none"
  fi
}

echo "Starting setup..."

# install any missing applications
check_and_install_apps "curl" "direnv" "tmux" "vim" "git"

# create development dir called "git" if none exists
if [ ! -d "${HOME}/git" ]; then
  mkdir "${HOME}/git"
fi

# setup ssh dir
if [ ! -d "${HOME}/.ssh/" ]; then
  mkdir "${HOME}/.ssh"
  chmod 700 "${HOME}/.ssh"
  touch "${HOME}/.ssh/authorized_keys"
  chmod 644 "${HOME}/.ssh/authorized_keys"
fi

# Create "environment" ssh file for use in .bashrc
if [ ! -f "${HOME}/.ssh/environment" ]; then
  touch "${HOME}/.ssh/environment"
fi

# clone tpm for tmux plugins
if [ ! -d "${HOME}/.tmux/plugins/tpm" ]; then
  git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
fi

# test for and install vim-plug if not installed
if [ ! -e "${HOME}/.vim/autoload/plug.vim" ] ; then
  echo "Installing vim-plug"
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

# Copy dotfiles to home dir.
cp "${DF_DIR}"/.bash* ~/
cp "${DF_DIR}/.gitconfig" ~/
cp "${DF_DIR}/.vimrc" ~/
cp "${DF_DIR}/.iftoprc" ~/

# Syntax highlighting for Hashicorp HCL
if [ ! -e "${HOME}/.vim/pack/jvirtanen/start" ] ; then
  mkdir -p ~/.vim/pack/jvirtanen/start
  cd ~/.vim/pack/jvirtanen/start
  git clone https://github.com/jvirtanen/vim-hcl.git
  cd
fi

# Copy .tmux.conf depending on tmux version
tmux_version=$(tmux -V | cut -d' ' -f2)
if [[ $tmux_version =~ ^2\.[6-9]|[7-9]\.[0-9a-z]+$ ]]; then
  cp "${DF_DIR}/.tmux_2.6.conf" ~/.tmux.conf
elif [[ $tmux_version =~ ^3\.[2-9]|[3-9]\.[0-9a-z]+$ ]]; then
  cp "${DF_DIR}/.tmux_3.2.conf" ~/.tmux.conf
else
  cp "${DF_DIR}/.tmux_3.conf" ~/.tmux.conf
fi

echo "Setup finished."
