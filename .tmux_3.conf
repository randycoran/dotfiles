################
#  TMUX.CONF   #
################

# remap prefix from 'C-b' to 'C-a'
unbind C-b
set-option -g prefix C-a
bind C-a send-prefix

# split panes using | and -
unbind '"'
unbind %
bind \\ split-window -h -c '#{pane_current_path}'  # Split panes horizontal
bind \- split-window -v -c '#{pane_current_path}'  # Split panes vertically

# enable mouse mode
set -g mouse on

# Toggle mouse on with ^B m
bind m \
  set -g mouse on \;\
  display 'Mouse: ON'

# Toggle mouse off with ^B M
bind M \
  set -g mouse off \;\
  display 'Mouse: OFF'

# renumber windows starting with 1
set -g base-index 1
set-option -g renumber-windows on
movew -r

################
#   MOVEMENT   #
################
# reference:
# https://zserge.com/posts/tmux/

# switch panes using Alt-arrow without prefix
bind -n M-h select-pane -L
bind -n M-l select-pane -R
bind -n M-k select-pane -U
bind -n M-j select-pane -D
bind -n M-Left select-pane -L
bind -n M-Right select-pane -R
bind -n M-Up select-pane -U
bind -n M-Down select-pane -D
bind -n "M-H" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -L; tmux swap-pane -t $old'
bind -n "M-J" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -D; tmux swap-pane -t $old'
bind -n "M-K" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -U; tmux swap-pane -t $old'
bind -n "M-L" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -R; tmux swap-pane -t $old'
bind -n "M-S-Left" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -L; tmux swap-pane -t $old'
bind -n "M-S-Down" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -D; tmux swap-pane -t $old'
bind -n "M-S-Up" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -U; tmux swap-pane -t $old'
bind -n "M-S-Right" run-shell 'old=`tmux display -p "#{pane_index}"`; tmux select-pane -R; tmux swap-pane -t $old'

# select windows with alt+1, alt+2, etc
bind -n M-1 select-window -t :1
bind -n M-2 select-window -t :2
bind -n M-3 select-window -t :3
bind -n M-4 select-window -t :4
bind -n M-5 select-window -t :5
bind -n M-6 select-window -t :6
bind -n M-7 select-window -t :7
bind -n M-8 select-window -t :8
bind -n M-9 select-window -t :9
bind -n M-0 select-window -t :0

# move or swap left/right in windows
bind -n M-. select-window -n
bind -n M-, select-window -p
bind -n M-< swap-window -t -1
bind -n M-> swap-window -t +1

# kill window or pane with ALT+X / ALT+x
bind -n M-x "kill-pane"
bind -n M-X "kill-window"

# set default to bash
# set-option -g default-command bash

# Create main window with bashrc file
#new -n MainWindow bash --login

# Reload tmux conf.
unbind r
bind r source-file ~/.tmux.conf\; display "Reloaded conf."

# toggle syncronize panes
unbind v
unbind V
bind v \
  setw synchronize-panes on \;\
  display 'Syncronize panes: ON'

bind V \
  setw synchronize-panes off \;\
  display 'Syncronize panes: OFF'

# swap window to the top
bind T swap-window -t 0

# bind control+shift+left/right to move windows up or down
bind -n C-S-Left swap-window -t -1
bind -n C-S-Right swap-window -t +1

# Select window to join
bind @ choose-window 'join-pane -h -s "%%"'

### Popups ###
# Need tmux 3.2 or later for popups.
#bind -n M-g popup -E "tmux new-session -A -s scratch"
#bind C-g popup -E "tmux attach ~/git"

### Status line ###

# show session window and pane
set -g status-left-length 20
set -g status-left "[#S:#I.#P] "

### Colors ###
set -g default-terminal "screen"

# default window selection
set -g status-style fg=white,bg=black

# non active windows
setw -g window-status-style fg=white,bg=black,dim

# currently active windows
setw -g window-status-current-style fg=black,bg=white,bright,underscore

# pane borders non active
set -g pane-border-style fg=white,dim

# pane borders active
set -g pane-active-border-style fg=green

################
#   PLUGINS    #
################

# List of TPM plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-sidebar'
set -g @plugin 'tmux-plugins/tmux-copycat'
set -g @plugin 'tmux-plugins/tmux-yank'
set -g @plugin 'tmux-plugins/tmux-open'

# tmux-continuum settings
set -g @continuum-save-interval '5'
set -g @continuum-restore 'on'
set -g @continuum-boot 'on'

# Initialize TMUX plugin manager (keep this line at the very bottom of tmux.conf)
run -b '~/.tmux/plugins/tpm/tpm'
