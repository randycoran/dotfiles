# General
alias cls='clear'
alias ll='ls -lAh'
alias mux='tmuxinator'

# Custom
alias todo-update='git add :/ ; git commit -m "Update $(date -Iseconds)" && git push'

# APT
alias apt-up-all='sudo apt-get update && sudo apt-get -y upgrade && sudo apt-get -y dist-upgrade && sudo apt-get -y autoremove && sudo apt-get -y clean'

# git specific
## http://www.codeblocq.com/2016/02/Best-git-log-aliases/
## https://www.youtube.com/watch?v=CAnQ4b0uais
alias glog='git log --oneline --graph'
alias glg="git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit"
alias gcam='git add -u && git commit -am'
alias gupdate='git pull --all && git pull --tags && git fetch --all && git fetch -p'
alias gpull='git pull -q --all && git pull -q --tags && git fetch -q --all && git fetch -q -p'
alias gchk='git checkout'
alias gstat='git status'
alias gacp='read -p "enter commit message: " MSG && git add -u && git commit -m "$MSG" && git push'
alias gsubup='git submodule update --init --recursive --jobs 8'
alias gsubpullall='git submodule update --remote --merge'

# k8s
alias kubectx='kubectl ctx'
alias kubens='kubectl ns'

# webcam
if which v4l2ucp >> /dev/null ; then
  alias webcamadjust='v4l2ucp'
fi
