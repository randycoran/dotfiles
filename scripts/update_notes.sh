#!/bin/bash

set -euo pipefail  # err exit, nounset, any failure in pipe fails

# Function to check for unstaged changes
check_unstaged_changes() {
  if [[ "$(git status --porcelain | wc -l)" -gt 0 ]]; then
    # Unstaged changes found
    return 0
  else
    # No unstaged changes found
    return 1
  fi
}

# Check if the current directory is a Git repository
if ! git rev-parse --is-inside-work-tree &> /dev/null; then
  echo "Error: This script must be run from within a Git repository."
  exit 1
fi

# Check for unstaged changes
if check_unstaged_changes; then
  # Perform the git commit
  git stash
  git pull --rebase
  git stash apply
  git add -A
  git commit -m "Update $(date -Iseconds)"
  git push
  echo "Commit performed successfully."
else
  echo "No unstaged changes found. No commit will be made."
  git pull --rebase
fi
