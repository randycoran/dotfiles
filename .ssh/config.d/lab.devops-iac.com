#====================================================
# lab rpi cluster / runner / nas / pve / mgmt
#====================================================

Host lab-*
  User                  rwcoran
  IdentitiesOnly        yes
  IdentityFile          ~/.ssh/lab/lab.devops-iac.com

Host lab-k3s-* lab-nas lab-runner lab-pve
  ProxyCommand          ssh -W %h:%p lab-nuc-mgmt

Host lab-nuc-mgmt
  HostName              lab.devops-iac.com
  localforward          8080 10.1.0.1:80
  localforward          8006 192.168.2.21:8006
  Port                  22086

### LOCAL / VPN ###

Host lab-local-nuc-mgmt
  HostName              nuc-mgmt.geek.lan

Host lab-local-runner
  HostName              runner.geek.lan

Host lab-local-pve
  HostName              pve.geek.lan

Host lab-local-k3s-01
  HostName              k3s-01

Host lab-local-k3s-02
  HostName              k3s-02

Host lab-local-k3s-03
  HostName              k3s-03

Host lab-local-k3s-04
  HostName              k3s-04

### REMOTE ###

Host lab-k3s-01
  HostName              k3s-01

Host lab-k3s-02
  HostName              k3s-02

Host lab-k3s-03
  HostName              k3s-03

Host lab-k3s-04
  HostName              k3s-04

Host lab-nas
  HostName              nas
  localforward          8443 192.168.2.20:443

Host lab-runner
  HostName              runner

Host lab-pve
  Hostname              pve
  User                  root
  localforward          8006 192.168.2.21:8006
