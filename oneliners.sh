#!/usr/bin/env bash

echo "Here is how to backup homedir into a tar.gz, excluding some dirs"
echo 'tar -zcv --exclude='dir1/' --exclude='patter*' --exclude='file2' -f /tmp/backup_$(date -I).tar.gz ${HOME}/'
